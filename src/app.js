import * as api from './services/api';

export class App {
	static inject = [api]
    vm = {};

    constructor(api){
    	this.api = api;
    	this.api.messages.on('created', newMsg => this.vm.data.push(newMsg));
    }

    activate() {
		return this.api.messages.find().then( data => this.vm = data);	
    }

    createMessage(){
    	this.api.messages.create(this.vm.message);
    }
    
}