import io from 'socket.io-client';
import * as feathers from 'feathers-client';

let baseUrl = 'http://localhost:3030';
let socket = io.connect(baseUrl);

let client = feathers.default()
	.configure(feathers.hooks())
	.configure(feathers.socketio(socket));
let messages = client.service('messages');

export {client,messages};
