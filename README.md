# Simple steps to integrate feathersjs API (real-time with socket.io) in Aurelia Application

1. au new
2. npm install socket.io-client feathers-client --save
3. Modify aurelia_project\aurelia.json (include above two packages in vendor-bundle.js dependencies)
4. Look at services/api.js (connecting aurelia app with feathersjs application with the help of feathers-client)
5. Look at the app.js code to use tha above api.js
6. app.html uses app.js (vew-model) to comunicate with feathersjs application
7. http://localhost:9000

# while deploying output of aurelia build (au build --env prod)
1. au build --env prod
2. deploy index.html and scripts\ to public folder of your feathersjs app.
3. http://localhost:3030
